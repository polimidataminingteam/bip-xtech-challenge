# Bip xTech Hands-on Challenge

Team Member:

- Wentai Zhang
- Chao Wu

[Dataset Download](https://drive.google.com/drive/folders/1BGLk_n5qs8xoe8Ahp5s9-1HsQECDvVSB?usp=sharing)

## Problem Description

### Context

- Our customer, one of Europe most important retailers would like to introduce a sales forecasting system to optimize promotions and warehouse
stocks.

### Objective

- The goal of the project is to provide a working forecasting model and for each store the sum of product sold for March and April.
- The provided training dataset contains daily information about number of sales, number of visitors, store information, weather information and 
demographic information.

### Activities

- Perform a data analysis aimed at analyzing trends and correlation between the available information
- Study and analyze solution/logics for the training set transformation and structuration 
- Design and train a forecast prediction model 
- Predict the sum of sales for each store for March and April.
